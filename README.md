hark (v2)
===

Listen on unused TCP ports and log messages / report metrics when we
see an unexpected connection. This can be used as a canary for
unexpected behavior on servers (detecting port scans).

